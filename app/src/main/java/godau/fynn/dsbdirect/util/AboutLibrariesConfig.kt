/*
 * DSBDirect
 * Copyright (C) 2020 Fynn Godau
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * This software is not affiliated with heinekingmedia GmbH, the
 * developer of the DSB platform.
 */
package godau.fynn.dsbdirect.util

import android.content.Context
import android.content.Intent
import godau.fynn.dsbdirect.BuildConfig
import godau.fynn.dsbdirect.R
import godau.fynn.librariesdirect.AboutDirectActivity
import godau.fynn.librariesdirect.model.Artwork
import godau.fynn.librariesdirect.model.ContextConsumer
import godau.fynn.librariesdirect.model.Imprint
import godau.fynn.librariesdirect.model.Library
import godau.fynn.librariesdirect.model.License
import godau.fynn.librariesdirect.model.OwnLicense

fun Context.getAboutLibrariesIntent(): Intent {
    return AboutDirectActivity.IntentBuilder(
        this, R.string.app_name, BuildConfig.VERSION_NAME
    ).apply {

        setIcon(R.mipmap.ic_launcher)
        setAppDeveloperName("Fynn Godau")

        setConsumer(object : ContextConsumer {
            override fun accept(t: Context) {
                Utility(t).stylize()
            }
        })

        setContent(
            arrayOf(
                Artwork(
                    what = getString(R.string.about_artwork_bixilon_what),
                    license = License.GNU_GPL_V3_OR_LATER_LICENSE,
                    author = "Moritz Zwerger"
                ),
                Artwork(
                    what = getString(R.string.about_artwork_braid_what),
                    license = License.GNU_GPL_V3_OR_LATER_LICENSE,
                    author = "The one with the braid"
                ),
                OwnLicense(
                    license = License.GNU_GPL_V3_OR_LATER_LICENSE,
                    foreword = null,
                    url = getString(R.string.uri_repository)
                ),
                Library(
                    name = "jsoup",
                    license = License.MIT_LICENSE,
                    foreword = "Copyright © 2009 - 2019 Jonathan Hedley (jonathan@hedley.net)",
                    author = "Jonathan Hedley",
                    plural = false,
                    url = "https://jsoup.org"
                ),
                Library(
                    name = "HorizontalPicker",
                    license = License.APACHE_20_LICENSE,
                    foreword = null,
                    author = "Blaž Šolar",
                    plural = false,
                    url = "http://blaz.solar/HorizontalPicker/"
                ),
                Library(
                    name = "Humanize",
                    license = License.APACHE_20_LICENSE,
                    foreword = null,
                    author = "mfornos",
                    plural = false,
                    url = "http://mfornos.github.io/humanize/"
                ),
                Library(
                    name = "TouchImageView",
                    license = License.MIT_LICENSE,
                    foreword = null,
                    author = "Michael Ortiz",
                    plural = false,
                    url = "https://github.com/MikeOrtiz/TouchImageView"
                ),
                Library(
                    name = "HtmlTextView",
                    license = License.APACHE_20_LICENSE,
                    foreword = null,
                    author = "Dominik Schürmann",
                    plural = false,
                    url = "https://github.com/sufficientlysecure/html-textview"
                ),
                Library(
                    name = "ShiftColorPicker",
                    license = License.MIT_LICENSE,
                    foreword = "The MIT License (MIT)\n\nCopyright (c) 2015 Bogdasarov Bogdan",
                    author = "Bogdasarov Bogdan",
                    plural = false,
                    url = "https://github.com/DASAR/ShiftColorPicker"
                ),
                Library(
                    name = "Picasso",
                    license = License.APACHE_20_LICENSE,
                    foreword = null,
                    author = "Square, Inc.",
                    plural = false,
                    url = "https://square.github.io/picasso/"
                ),
                Library(
                    name = "Conscrypt",
                    license = License.APACHE_20_LICENSE,
                    foreword = """This product contains a modified portion of `Netty`, a configurable network
                    stack in Java, which can be obtained at:
                    
                      * LICENSE:
                        * licenses/LICENSE.netty.txt (Apache License 2.0)
                      * HOMEPAGE:
                        * http://netty.io/
                    
                    This product contains a modified portion of `Apache Harmony`, modular Java runtime,
                    which can be obtained at:
                    
                      * LICENSE:
                        * licenses/LICENSE.harmony.txt (Apache License 2.0)
                      * HOMEPAGE:
                        * https://harmony.apache.org/""",
                    author = "The Android Open Source Project",
                    plural = false,
                    url = "https://conscrypt.org/"
                ),
                Library(
                    name = "librariesDirect",
                    license = License.CC0_LICENSE,
                    foreword = null,
                    author = "Fynn Godau",
                    plural = false,
                    url = "https://codeberg.org/fynngodau/librariesDirect"
                ),
                Library(
                    name = "eltern-portal.org API",
                    license = License.GNU_GPL_V3_OR_LATER_LICENSE,
                    foreword = null,
                    author = "Moritz Zwerger",
                    plural = false,
                    url = "https://gitlab.bixilon.de/bixilon/eltern-portal.org-api"
                ),
                Imprint(
                    """
                Fynn Godau
                Stößelstraße 6
                97422 Schweinfurt
                Deutschland
                
                fynngodau@mailbox.org
                +49 9721 730335
                
                Umsatzsteuer-Identifikationsnummer (VAT identification number): DE347187327
                
                Plattform der EU zur außergerichtlichen Streitbeilegung
                (EU platform for Online Dispute Resolution):
                https://ec.europa.eu/consumers/odr/
                """.trimIndent()
                )
            )
        )

    }.build()
}
