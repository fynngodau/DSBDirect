/*
 * DSBDirect
 * Copyright (C) 2021 Fynn Godau
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * This software is not affiliated with heinekingmedia GmbH, the
 * developer of the DSB platform.
 */

package godau.fynn.dsbdirect.test;

import godau.fynn.dsbdirect.model.entry.Entry;
import godau.fynn.dsbdirect.model.entry.EntryField;

import java.io.*;
import java.util.ArrayList;

public class TableRenderer {

    public static void renderTo(ArrayList<Entry> entries, PrintStream stream) {
        int[] measures = new int[EntryField.values().length];

        for (EntryField field : EntryField.values()) {

            measures[field.ordinal()] = field.name().length();

            for (Entry entry : entries) {
                if (entry.get(field) != null)
                    measures[field.ordinal()] =
                            Math.max(entry.get(field).length(), measures[field.ordinal()]);
            }

            stream.print("| " + lengthify(field.name(), measures[field.ordinal()]) + " ");
        }

        stream.println("|");


        for (Entry entry : entries) {

            for (EntryField field : EntryField.values()) {
                stream.print("| " + lengthify(
                        entry.get(field) == null? "" : entry.get(field),
                        measures[field.ordinal()]) + " ");

            }
            stream.println("|");
        }
    }

    public static void renderToLog(ArrayList<Entry> entries) {
        renderTo(entries, System.out);
    }

    public static String renderToString(ArrayList<Entry> entries) {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        PrintStream stream = new PrintStream(output);
        renderTo(entries, stream);

        return output.toString();
    }

    private static String lengthify(String s, int l) {
        while (s.length() < l) {
            s += " ";
        }
        return s;
    }
}
