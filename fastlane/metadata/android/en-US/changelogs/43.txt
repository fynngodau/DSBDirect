Bugfix release.

* Fix crash on low Android versions
* Fix notifications on high Android versions
